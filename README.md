Xolominer Parameters
====================

A simple UI for configuring and running Xolokrams Primecoin Miner
-----------------------------------------------------------------
For Windows

Setup:
------
Create a folder. 
Inside put the xolominer-parameters.exe file and a copy of the primeminer_*.exe file for your system (32 bit or 64 bit).

See here for download links for Xolokrams primeminer: <http://www.ppcointalk.org/index.php?topic=485.0>

Usage:
------
On first run you will be asked to fill in the parameters.
every parameter in the 'Pool Parameters' section needs to have a value. Xolominer wil not run without them.
the 'Arguments' will default to standard values if you don't fill them in

Once you are happy with the settings you can 'Save' or 'Launch!'.
the script will determine if you are running a 64 bit OS or not and start the appropriate primeminer.exe

The settings will be saved in a file called settings.ini in the same directory as the main xolominer-parameters.exe script

On subsequent runs, xolominer will be run straight off using the saved parameters. 
to view the UI again, press 'WindowsKey+Ctrl+W' at any time.
	
Notes:
------
Built using AutoHotkey <http://autohotkey.com>
Compiled using the bundled ahk2exe script.
