﻿;========================================================================================
;
; Xolominer Parameters
;
; A simple UI for configuring and running Xolokrams Primecoin Miner
;
; Setup - Create a folder. 
; 	Inside put the xolominer-parameters.exe file and a copy of the xolominer.exe file for your system (32 bit or 64 bit)
;
; Usage - On first run you will be asked to fill in the parameters.
; 	every parameter in the 'Pool Parameters' section needs to have a value. Xolominer wil not run without them
; 	the 'Arguments' will default to standard values if you don't fill them in
;
; 	Once you are happy with the settings you can 'Save' or 'Launch!'.
;	the script will determine if you are running a 64bit OS or not and start the appropriate xolominer executable
;
; 	The settings will be saved in an ini file called settings.ini in the same directory as the main xolominer-parameters.exe script
;
;	On subsequent runs, xolominer will be run straight off using the saved parameters. 
;	to view the UI again, press 'WindowsKey+Ctrl+W' at any time.
; 
;========================================================================================

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;Build the UI. This gets held in memory and shown or hidden depnding on the task
Gui, Font, Arial s12
Gui, Add, Text, w450 Center cBlue, Xolominer Parameters
Gui, Font, s10
Gui, Add, GroupBox, w450 h130 x10 y40 Section, Pool Parameters
Gui, Font, s9
Gui, Add, Text, w100 Right x20 y70 Section, Payout Address:
Gui, Add, Edit, w310 vPayoutAddress ys-3,
Gui, Add, Text, w100 Right xs y+15 Section, Pool Address:
Gui, Add, Edit, w120 vPoolAddress ys-3,
Gui, Add, Text, w65 Right ys x+0, Port:
Gui, Add, Edit, w110 ys-3,
Gui, Add, UpDown, vPoolPort 0x80 Range0-3000000,
Gui, Add, Text, w100 Right xs y+15 Section, Pool Fee:
Gui, Add, Edit, w120 ys-3,
Gui, Add, UpDown, vPoolFee 0x80 
Gui, Add, Text, w65 Right ys x+0, Miner id:
Gui, Add, Edit, w110 ys-3,
Gui, Add, UpDown, vMinerId 0x80 Range0-65000,
Gui, Font, s10
Gui, Add, GroupBox, w450 h115 x10 y180 Section, Arguments
Gui, Font, s9
Gui, Add, Text, w110 Right x20 y210 Section, Proc Limit:
Gui, Add, Edit, w80 ys-3,
Gui, Add, UpDown, vGenProcLimit 0x80 Range1-32
Gui, Add, Text, w110 Right ys, Sieve Extensions:
Gui, Add, Edit, w80 ys-3,
Gui, Add, UpDown, vSieveExtensions 0x80 
Gui, Add, Text, w110 Right xs Section, Bits:
Gui, Add, Edit, w80 ys-3,
Gui, Add, UpDown, vBits 0x80 
Gui, Add, Text, w110 Right ys, Target Initial Length:
Gui, Add, Edit, w80 ys-3,
Gui, Add, UpDown, vTargetInitialLength 0x80 
Gui, Add, Text, w110 Right xs Section, Sieve Percentage:
Gui, Add, Edit, w80 ys-3,
Gui, Add, UpDown, vSievePercentage 0x80 
Gui, Add, Text, w110 Right ys, Sieve Size:
Gui, Add, Edit, w80 ys-3,
Gui, Add, UpDown, vSieveSize 0x80 Range1-200000000
Gui, Add, Button, w150 h30 xs y+25 Section, Save
Gui, Add, Button, w150 h30 ys x+130, Launch!

;Try to run the miner
RunMiner()
Return  

;Save and close the application
GuiClose:
SaveParams()
ExitApp

;Respond to the 'Save' button
ButtonSave:
SaveParams()
Return

;Respond to the 'Launch!' button
ButtonLaunch!:
SaveParams()
HideUI()
RunMiner()
Return

;Set the hotkey combination to show the UI
#^w::ShowUI()

;Show the UI. Kill the xolominer script if it exists
ShowUI() {
	Process, Close, % StorePID() 
	LoadUIParams()
	Gui, Show, AutoSize, Xolominer Parameters
	Return
}

;Hide the UI
HideUI() {
	Gui, Show, Hide
	Return
}

;Get the parameters saved in the settings.ini file and build the params object
GetParams() {
	If !FileExist("settings.ini")
	{
		FileAppend, , settings.ini
		ShowUI()
		Return
	}
	params := object()
	ReadParam("PayoutAddress", params)
	ReadParam("PoolAddress", params)
	ReadParam("PoolPort", params)
	ReadParam("PoolFee", params)
	ReadParam("MinerId", params)
	ReadParam("GenProcLimit", params)
	ReadParam("SieveExtensions", params)
	ReadParam("Bits", params)
	ReadParam("TargetInitialLength", params)
	ReadParam("SievePercentage", params)
	ReadParam("SieveSize", params)
	Return, params
}

;read a given value from the settings.ini file and update the object
ReadParam(key, object) {
	IniRead, value, settings.ini, settings, % key, % A_Space
	object.Insert(key,value)
	Return, object
}

;save a given value back to the settings.ini file
SaveParam(key, value, object) {
	IniWrite, % value, settings.ini, settings, % key
}

;using the params object, populate the UI with the saved values
LoadUIParams() {
	enum := GetParams()._NewEnum()
	while enum[k,v]
	{
		GuiControl, , % k, % v 
	}
	Return
}

;save all the entered values from the UI
SaveParams() {
	params := GetParams()
	enum := params._NewEnum()
	Gui, Submit, Nohide
	while enum[k,v]
	{
		params.Insert(k, %k%)
		SaveParam(k, %k%, params)
	}
	Return
}

;save the process ID generated when the miner is started
StorePID(PID="") {
	static _PID
	If PID
	{
		_PID := PID
	}
	Return _PID
}

;check for a internet connection
CheckInternet(flag=0x40) {
	Return DllCall("Wininet.dll\InternetGetConnectedState", "Str", flag, "Int", 0)
}

;run the miner
RunMiner() {
	params := GetParams()
	If (params.PayoutAddress = "") || (params.PoolAddress = "") || (params.PoolPort = 0) || (params.PoolFee = 0) || (params.MinerId = "")
	{
		ShowUI()
		MsgBox, 0, Xolominer Parameters, All Pool Parameters must have a value., 5
		Return
	}
	miner := A_Is64bitOS ? "primeminer_x64.exe" : "primeminer_x32.exe"
	If !FileExist(miner)
	{
		MsgBox, 0, Xolominer Parameters, Could not find the Xolominer executable '%miner%'.`n`nPlease put a copy of %miner% in`n%A_ScriptDir%
		Return
	}
	If !CheckInternet()
	{
		ShowUI()
		MsgBox, 0, Xolominer Parameters, No internet connection., 5
		Return
	}
	Run, % miner . " -pooluser=" . params.PayoutAddress . " -poolpassword=0 -poolip=" . params.PoolAddress . " -poolport=" . params.PoolPort . " -minerid=" . params.MinerId . " -poolfee=" . params.PoolFee . " -GenProcLimit=" . params.GenProcLimit . ((params.Bits=0) ? " -bits=" . params.Bits : "") . ((params.SieveExtensions=0) ? " -SieveExtensions=" . params.SieveExtensions : "") . ((params.TargetInitialLength=0) ? " -TargetInitialLength=" . params.TargetInitialLength : "") . ((params.SievePercentage=0) ? " -SievePercentage=" . params.SievePercentage : "") . ((params.SieveSize=1) ? " -SieveSize=" . params.SieveSize : ""), , max, PID
	StorePID(PID)
	Return
}